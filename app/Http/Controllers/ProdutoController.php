<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProdutoController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    private function registrarLog($function, $acao = ""){
        if (Auth::check()) {
            $user = Auth::user();
            Log::channel('vesti')->info("Usuario {$user->name} | Funcao: $function .| Acao: $acao .");
        }                        
        else{
            Log::channel('vesti')->warning("Acesso realizado sem validacao! | Funcao: $function .| Acao: $acao .");
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('produto.index');       
    }

    /**
     * Lista todos os produtos em formato Json.
     *
     * @return \Illuminate\Http\Response
     */
    public function listarProdutoJson()
    {
        $produtosList = Produto::all();        
        return $produtosList->toJson();
    }

    public function listarTamanhoProdutoJson()
    {
        //        
        $tamanho = [
            ['tipo'=>'PEQUENO'], ['tipo'=>'MEDIO'], ['tipo'=>'GRANDE'], ['tipo'=>'G. GRANDE'], ['tipo'=>'PLUZ SIZE']
        ];
        $this->registrarLog(__FUNCTION__);
        return json_encode($tamanho);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produto = new Produto();
        $produto->codigo = $request->input("codigo");
        $produto->nome = $request->input("nome");
        $produto->composicao = $request->input("composicao");
        $produto->tamanho = $request->input("tamanho");
        $produto->quantidade = $request->input("quantidade");

        $produto->save();     
        $this->registrarLog(__FUNCTION__);
        return $produto->toJson();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->registrarLog(__FUNCTION__, "Usuario consultou produto de id $id.");
                
        $produto = Produto::find($id);
        if (isset($produto)) {
            return json_encode($produto);            
        }
        return response('Produto não encontrado', 404);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::find($id);
        $produto->codigo = $request->input("codigo");
        $produto->nome = $request->input("nome");
        $produto->composicao = $request->input("composicao");
        $produto->tamanho = $request->input("tamanho");
        $produto->quantidade = $request->input("quantidade");

        $this->registrarLog(__FUNCTION__, "Usuario alterou produto de id $id."); 

        $produto->save();        
        return $produto->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        if (isset($produto)) {
            $produto->delete();
            $this->registrarLog(__FUNCTION__, "Usuario exclui produto de id $id."); 
        }        
    }
}
