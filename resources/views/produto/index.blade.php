@extends('layouts.app')

@section('content')

<h3>Produtos</h3>

<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Cadastro de Produtos</h5>

        <table class="table table-ordered table-hover" id="tabelaProdutos">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>Composição</th>
                    <th>Tamanho</th>
                    <th>Quantidade</th>                    
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <button class="btn btn-sm btn-primary" role="button" onClick="novoProduto();" id="btnNovoProduto">Novo produto</a>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="dlgProdutos">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content">
            <form class="form-horizontal" id="formProduto">
                <div class="modal-header">
                    <h5 class="modal-title">Novo produto</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" class="form-control">

                    <div class="form-group">
                        <label for="codigoProduto" class="control-label">Código</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="codigoProduto" required placeholder="Código do produto" oninput="this.value = this.value.toUpperCase();" maxlength="10">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="nomeProduto" class="control-label">Nome do Produto</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nomeProduto" required placeholder="Nome do produto" maxlength="30">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="composicaoProduto" class="control-label">Composição</label>
                        <div class="input-group">                            
                            <textarea class="form-control" required id="composicaoProduto"  form="formProduto" placeholder="Exemplo(ALGODÃO:80% LINHO:20%)" oninput="this.value = this.value.toUpperCase();" maxlength="200"></textarea>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label for="tamanhoProduto" class="control-label">Tamanho</label>
                        <div class="input-group">
                            <select class="form-control" id="tamanhoProduto" required>
                            </select>    
                        </div>
                    </div>                                                    
                    
                    <div class="form-group">
                        <label for="quantidadeProduto" class="control-label">Quantidade</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="quantidadeProduto" required placeholder="Quantidade do produto">
                        </div>
                    </div>                    

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$('#dlgProdutos').hide();">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>  

@endsection 
@section('javascript')

<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    
    function novoProduto() {
        $('#id').val('');
        $('#codigoProduto').val('');
        $('#nomeProduto').val('');
        $('#composicaoProduto').val('');
        $('#tamanhoProduto').val('');
        $('#quantidadeProduto').val('');
        $('#dlgProdutos').show();
    }
    
    function carregarTamanhoProduto() {
        $.getJSON('/api/listarTamanhoProdutoJson', function(data) { 
            $('#tamanhoProduto').append('<option value="" selected>Selecione o tamanho</option>');
            for(i=0;i<data.length;i++) {
                opcao = '<option value ="' + data[i].tipo + '">' + 
                    data[i].tipo + '</option>';
                $('#tamanhoProduto').append(opcao);
            }
        });
    }
    
    function montarLinha(p) {
        let idRow = `rowTableProdutos${p.id}`;
        let linha = "<tr id=" + idRow + ">" +
            "<td>" + p.codigo + "</td>" +
            "<td>" + p.nome + "</td>" +
            "<td>" + p.composicao + "</td>" +
            "<td>" + p.tamanho + "</td>" +
            "<td>" + p.quantidade + "</td>" +
            "<td>" +
              `<button class="btn btn-sm btn-primary" onclick="editar(${p.id},'${idRow}')"> Editar </button> ` +
              `<button class="btn btn-sm btn-danger" onclick="remover(${p.id}, '${idRow}')"> Apagar </button> ` +
            "</td>" +
            "</tr>";
        return linha;
    }
    
    function editar(id) {
        $.getJSON('/api/produto/'+id, function(data) {             
            $('#id').val(data.id);
            $('#codigoProduto').val(data.codigo);
            $('#nomeProduto').val(data.nome);
            $('#composicaoProduto').val(data.composicao);
            $('#tamanhoProduto').val(data.tamanho);
            $('#quantidadeProduto').val(data.quantidade);            
            $('#dlgProdutos').show();            
        });        
    }
    
    function remover(id, idRow) {
        $.ajax({
            type: "DELETE",
            url: "/api/produto/" + id,
            context: this,
            success: function() {                
                linha = $("#"+idRow);
                linha.remove();
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
    
    function carregarProdutos() {        
        $.getJSON('/api/listarProdutoJson', function(produtos) {           
            for(i=0;i<produtos.length;i++) {
                linha = montarLinha(produtos[i]);
                $('#tabelaProdutos>tbody').append(linha);
            }
        });        
    }
    
    function criarProduto() {
        produto = { 
            codigo: $("#codigoProduto").val(), 
            nome: $("#nomeProduto").val(), 
            composicao: $("#composicaoProduto").val(), 
            tamanho: $("#tamanhoProduto").val(), 
            quantidade: $("#quantidadeProduto").val(), 
        };
        $.post("/api/produto", produto, function(data) {
            produtoJson = JSON.parse(data);
            linha = montarLinha(produtoJson);            
            $('#tabelaProdutos>tbody').append(linha);            
        });
    }
    
    function salvarProduto(idRow) {
        prod = { 
            id : $("#id").val(), 
            codigo : $("#codigoProduto").val(), 
            nome: $("#nomeProduto").val(), 
            composicao: $("#composicaoProduto").val(), 
            tamanho: $("#tamanhoProduto").val(), 
            quantidade: $("#quantidadeProduto").val() 
        };
        $.ajax({
            type: "PUT",
            url: "/api/produto/" + prod.id,
            context: this,
            data: prod,
            success: function(data) {
                prod = JSON.parse(data);
                linha = $(idRow);
                linha[0].cells[0].textContent = prod.codigo;
                linha[0].cells[1].textContent = prod.nome;
                linha[0].cells[2].textContent = prod.composicao;
                linha[0].cells[3].textContent = prod.tamanho;
                linha[0].cells[4].textContent = prod.quantidade;
            },
            error: function(error) {
                console.log(error);
            }
        });        
    }
    
    $("#formProduto").submit( function(event){ 
        event.preventDefault(); 
        if ($("#id").val() != '')
            salvarProduto(`#rowTableProdutos${$("#id").val()}`);
        else
            criarProduto();
            
        $("#dlgProdutos").hide();
    });
    
    $(function(){
        carregarProdutos();
        carregarTamanhoProduto();        
    })
    
</script>
@endsection