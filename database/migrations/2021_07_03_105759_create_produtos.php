<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo',10);
            $table->string('nome',30);
            $table->string('composicao',200);//tipo de material que compõe a roupa (algodão, poliéster)
            $table->string('tamanho', 30); //PEQUENA, GRANDE, EXTRA GRANTE
            $table->integer('quantidade'); //quantidade do produto em estoque
            $table->softDeletes();
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
