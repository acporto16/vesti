<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdutoController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('produto', ProdutoController::class);
   
Route::get('listarProdutoJson', [ProdutoController::class, 'listarProdutoJson']);     

/*
Route::get('listarProdutoJson', [ProdutoController::class, 'listarProdutoJson'])->middleware('auth');  //Logado dá o erro: Failed to load data      
Route::middleware('auth')->get('listarProdutoJson', [ProdutoController::class, 'listarProdutoJson']);  //Logado dá o erro: Failed to load data    
*/
Route::get('listarTamanhoProdutoJson', [ProdutoController::class, 'listarTamanhoProdutoJson']);  //funciona mas não valida autenticação     

