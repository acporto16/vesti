<?php

use App\Http\Controllers\ProdutoController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login') ;       
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/produto', [App\Http\Controllers\ProdutoController::class, 'index'])->name('produto');

//Route::resource('produto', ProdutoController::class)->middleware('auth')->middleware('auth');

Route::middleware('auth')->get('/produto', [ProdutoController::class, 'index']);

// Route::get('logout', function (){
//     Auth::logout();
//     return redirect('login');
//     });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
