## PREPARAÇÃO DO BANCO DE DADOS

Criar o schema de banco de dados MYSQL por meio do instruções abaixo:

- Conecte ao banco de dados MYSQL.

- Executar o comando: create database vesti character set UTF8mb4 collate utf8mb4_bin.

- Definir a usuário e senha conforme conveniência, o projeto está usando o usuário root e sem senha.

# PREPARAÇÃO DO AMBIENTE

- Clonar o projeto proveniente do git (https://gitlab.com/acporto16/vesti.git).

- Antes da execução do ambiente é necessário a geração da pasta vendor. A mesma não está presente nos fontes do git. Para geração da pasta vendor execute o comando "composer install" dentro do projeto.

- Com os fontes já disponíveis, acessar o diretório vesti e executar o seguinte comando: php artisan serve.

- Altere o arquivo .env na seção banco de dados o host do servidor, usuário e senha conforme as configurações do servidor do seu banco de dados.
  Obs.: O arquivo .env fica residente na raiz do projeto VESTI.

- No terminal, acesse a pasta do projeto VESTI. Para criar a tabela Produtos, execute o comando migrate a seguir: php artisan migrate.
  Obs.: A tabela está no plural por questões relacionadas ao framework Laravel, principalmente na parte de mapeamento Objeto/Relacional.

## PONTOS DE MELHORIA E CORREÇÃO

- A quantidade de produtos deve refletir o estoque da loja. O campo não poderá ser alimentado diretamente pelo cadastro de produto. 

- Analisar com a equipe do projeto se compensa, ou se é necessário, criar uma estrutura de composição do produto (ALGODÃO: 70%, POLIESTER: 30%). Nestes termos, deverá ser criado as tabelas Tipo Composição e Composição do Produto, bem como associar a segunda à tabela Produtos. Talvez seja necessário o envolvimento do PO, pois estas alterações tem impacto direto na usuabilidade do sistema. *

- Pelas mesmas motivações explicitadas acima, o campo tamanho também poderia ser tabelado, e uma funcionalidade poderia ser criada para que o usuário possa cadastrar os tipos de tamanho (PEQUENA, GRANDE, MÉDIA, EXTRA GRANDE, PLUS SIZE, etc). *

* Obs.: O líder do projeto deverá avaliar se compensa ou não levar o assunto para debate com o PO.

- Adicionar tratamento de exceção e exibição de mensagens customizadas para o usuário, bem como a gravação de log com os detalhes do erro (exception trace).

- A recuperação da senha não foi testada.

- Falha de segurança. A chamada direta das API's (listarTamanhoProdutoJson e listarProdutos) pela URL não está validando o acesso do usuário. Ao proteger as rotas usuando o middleware('auth:api'), o sistema não obtém os dados mesmo com o acesso realizado. A falha não chega a ser grave, pois os métodos não alteram dados do sistema. 

- Para o item acima, a solução seria o redesenvolvimento das API's utilizando a biblioteca Passport(https://www.positronx.io/laravel-rest-api-with-passport-authentication-tutorial/).

- Seria mais interessante o desenvolvimento de uma solução de IAM (Gestão de Identidade de Acessos) separado do sistema VESTI. Neste novo projeto seria definido o cadastro de usuários, sistemas, perfis, controle de log de acesso etc. Este novo sistema ficaria num servidor a parte com proxy reverso.

- As API's foram desenvolvidas para que fossem utilizadas em sistemas que utilizam tecnologias diferentes do PHP, a exemplo aplicativos mobile. Se faz necessário testes neste sentido.






